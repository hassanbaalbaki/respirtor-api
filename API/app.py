from flask import Flask


IP_ADD = '0.0.0.0'
APP_PORT = 5620


app = Flask(__name__)
app.secret_key = "The respirator secret key is 123456"

app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024
app.config['IP_ADD'] = IP_ADD
app.config['APP_PORT'] = APP_PORT