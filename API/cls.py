from collections import deque


import json
from json import JSONEncoder

from scripts import SmartJson


#Input output
#percentage is used to set the value
class Control:
    def __init__(self, percentage, realValue, label: str):
        self.percentage = percentage
        self.realValue = realValue
        self.label = label
        
   
    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__)

    @classmethod
    def from_json(cls,data):
       return cls(**data)


#Sensor information  
class Sensor:
    def __init__(self, id, label, realValue):
        self.realValue = realValue
        self.label = label
        self.id=id
        self.history= deque(120*[0], 120)

    def update(self, newValue):
        self.realValue = newValue
        self.history.appendleft(newValue)

    def toJson(self):
        #return json.dumps(self, default=lambda o: o.__dict__)
        #return SmartJson(self.history).serialize
        #return json.dumps({ 'id':self.id , 'label': self.label, 'realValue': self.realValue, 'history': list(self.history)},indent=2)
        return json.dumps(self, cls=SensorEncoder)

    @classmethod
    def from_json(cls,data):
       return cls(**data) 

# subclass JSONEncoder
class SensorEncoder(JSONEncoder):
        def default(self, o):
            if isinstance(o,deque):
                return list(o)
            
            return o.__dict__