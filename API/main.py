import os
import urllib.request
from app import app
from flask import Flask, request, redirect, jsonify,Response
from werkzeug.utils import secure_filename
from cls import Sensor
from cls import Control

from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.backends.backend_svg import FigureCanvasSVG


from matplotlib.figure import Figure
import io
import random

sensorList = []
IE = Sensor(1,"I/E",0.614)
sensorList.append(IE)

volume= Sensor(2,"Volume",1.1)
sensorList.append(volume)


@app.route('/api/sensor/', methods=['GET'])
def get_all_sensor_data():
     return jsonify(sensordat=[sensor.toJson() for sensor in sensorList ])



@app.route('/api/sensor/<sensorID>/', methods=['GET'])
def get_sensor_data(sensorID:int):

    sensor = get_sensor(sensorID)

    if sensor is None :
        notFoundStr = "Unrecognised sensor, No sensor having the the requested ID({}) ".format(sensorID)
        resp = jsonify({'message': notFoundStr})
        resp.status_code = 400
        return resp
    else:
        return sensor.toJson()
    


@app.route('/api/sensor/<sensorID>/', methods=['POST'])
def set_sensor_data(sensorID):
    # check if the username is given
    if 'newVal' not in request.values:
        resp = jsonify({'message': 'A newVal representing the new value, is required in the request body'})
        resp.status_code = 400
        return resp

    sensor = get_sensor(sensorID)

    if sensor is None :
        notFoundStr = "Unrecognised sensor, No sensor having the the requested ID({}) ".format(sensorID)
        resp = jsonify({'message': notFoundStr})
        resp.status_code = 400
        return resp
    else:
        sensor.update(request.values['newVal'])
        return jsonify({'message': 'The value of the sensor have been update'})
   

@app.route('/plot/sensor/<sensorID>/')
def plot_png(sensorID):
    """ renders the plot on the fly.
    """

    sensor = get_sensor(sensorID)

    if sensor is None :
        return "Sensor ID({}) is not recongnised".format(sensorID)
    else:
            fig = Figure()
            axis = fig.add_subplot(1, 1, 1)
            x_points = range(len(sensor.history))
            axis.plot(x_points,sensor.history)

            output = io.BytesIO()
            FigureCanvasAgg(fig).print_png(output)
            return Response(output.getvalue(), mimetype="image/png")
    

@app.route('/api/controler/', methods=['POST'])
def setControlor():
    # check if the controlorID is given
    if 'controlorID' not in request.values:
        resp = jsonify({'message': 'A controlorID is required in the request'})
        resp.status_code = 400
        return resp
    # check if the username is given
    if 'percentage' not in request.values:
        resp = jsonify({'message': 'A boxid is required in the request'})
        resp.status_code = 400
        return resp
    sendToControler(request.values['controlorID'],request.values['percentage'])
    resp = jsonify({'message': 'File successfully uploaded'})
    resp.status_code = 201
    return resp
    
def sendToControler(controlorID,percentage):
    #TODO
    return

def randValueArray(num_points):
    return [random.randint(1, 100) for i in range(num_points)]


#return the data of specified sensor
def get_sensor(sensorID):
    for sensor in sensorList:
        if int(sensor.id) == int(sensorID):
            return sensor
    return None

@app.route("/simpleUI/")
def hello():
    str =  '<h1>Simple interface could be used to get the data</h1>'
    for sensor in sensorList:
        str += "<br> {}-<b>I{} :</b> {: f}  ".format(sensor.id,sensor.label,sensor.realValue)
        str += "<br> <img src='http://localhost:5620/plot/sensor/{}/'> ".format(sensor.id)
    return str

if __name__ == "__main__":
    app.run(host = app.config['IP_ADD'], port = app.config['APP_PORT'],debug=True )
