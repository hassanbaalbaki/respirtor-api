import time
import requests #pip install requests
import serial #pip install pyserial

arduino= serial.Serial('COM3',9600)

def uploadSensorValue(idSensor,newVal):
    url = 'https://127.0.0.1:5620//api/sensor/'+idSensor+'/'
    postData = {'newVal': newVal}
    x = requests.post(url, data = postData)
    print(x)
    
def updateS1():
  newVal = (1.2 * random.randint(1, 100))/100 #TODO read over  arduino serial
  uploadSensorValue( 1,newVal)
  
def updateS2():
  newVal = (0.6 random.randint(1, 100))/100 #TODO read from GIPO or an arduino serial
  uploadSensorValue(2,newVal)
 
def readAndUploadSensor(idSensor):
    newVal= readFromSerial(idSensor)
    uploadSensorValue( idSensor,newVal)
 
def readFromSerial(idSensor):
    arduino.write(idSensor)
    return arduino.readline()

def main():
    while True:
      updateS1() 
      updateS2()
      
      time.sleep(0.1)